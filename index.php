<!DOCTYPE html SYSTEM "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title>Taylor &amp; Francis Journals Production</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
    <style type="text/css">
        body {
            background-color: #EBF4FF;
        }
        
        img {
            margin: auto;
            position: absolute;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
        }
        
        .logo {
            width: 100%;
            height: 100%;
        }
        
        .links {
            position: absolute;
            top: 70%;
            left: 50%;
            transform: translateX(-50%);
            text-align: center;
            font-family: 'Raleway';
            font-size: 30px;

        }
        
        svg {
            margin: auto;
            position: absolute;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
            width: 30%;
            height: 30%;
        }
    </style>
</head>

<body>
    <div>
        <div class="logo">
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="-1.144 -1.148 54 54" enable-background="new -1.144 -1.148 54 54" xml:space="preserve">
                <defs></defs>
                <path fill="#004FA3" d="M25.589,0C11.46,0,0,11.455,0,25.588c0,14.137,11.46,25.584,25.589,25.584
                      c14.128,0,25.588-11.447,25.588-25.584C51.177,11.455,39.717,0,25.589,0z" />
                <polygon fill="#FFFFFF" points="18.757,25.904 33.042,25.904 33.549,24.711 18.255,24.711 18.757,25.904 " />
                <polygon fill="#FFFFFF" points="17.987,24.066 33.818,24.066 34.326,22.872 17.479,22.872 17.987,24.066 " />
                <path fill="#FFFFFF" d="M9.967,17.504c0,0-3.058,6.771,0.042,6.849C10.008,24.353,12.881,24.449,9.967,17.504z" />
                <path fill="#FFFFFF" d="M28.443,15.58c0,0-7.363,3.231-2.741,6.193C25.702,21.773,25.493,19.379,28.443,15.58z" />
                <path fill="#FFFFFF" d="M38.69,26.49c-4.98,0-26.848,0-26.848,0v-0.661H8.271v0.661H7.458l0.89,2.09h4.479
                      c2.455,4.7,7.369,7.895,13.036,7.895c3.954,0,7.537-1.558,10.182-4.084c6.646,0.521,8.271-7.434,8.271-7.434L38.69,26.49z
                      M37.03,31.339c0.496-0.571,0.95-1.188,1.35-1.839c0,0,0.71-1.104,1.093-2.144l3.29-0.871C40.84,30.951,37.885,31.328,37.03,31.339z
                      " />
            </svg>
        </div>
        <div class="links">
            <a href="http://production.tfjournals.com/">Production Hub</a>
        </div>

        <!-- img alt="Taylor &amp; Francis"
            src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAddSURBVHjaYmQYQKARucwBSIGwPBArALEBEAsQ0PYACV8E4gs3lkcdGCg/AAQQI50DDBRAAUBsDw04agJQIG4E4g3AAH1ALz8BBBAjHQJNARpo+dBURg9wAYgXAvECYGB+oKVFAAHESOOAqwfiBIaBBQuAuJFWqRIggBiHccDRJSABAoiRigEnAA24AobBDRqBeAK1sjZAADFSKfBAZVw/Hcs4SgEoFSZSo/YGCCDGEZLqcAFQSiykxACAAGKkIPBAqW09tO02lAGoxnYkN0sDBBAjmYEHCrT9RDR6hwr4AA3EC6RqBAggJjICL2GYBR4D1C/7oQmDJAAQQIxkBN58huELSE6JAAHEOBp4lAUiQAAxEhl4BsMw21IlEAECiJGIwAPVtudHUOAhtxUNCdXOAAHEREQ7b/0IDDwGaKdgPSFFAAFEqBauHwbtPEqAAzARNeBTABBAjAS6Z+sZRgEDtDzE2u0DCCAmPFm3fzTc4ABn6wMggJjwZF2F0XBDlIe4sjJAADHiqHXvj4YZ1qaNInqtDBBATDhS3yjA3t3DKNYAAohxNPWRDBSRR7UBAohpNPWRDPKROQABxIhW874fzC4PtFNk+PTtN8PeM08GTVkIEEDIKTBhsAacmZYYw/p2D4YAe6WBDjxYWRgA4wAEEBOupDlYQGWsEcPCGmcGaVFuhqoZJwaLs+JhDIAAYoJmXwNy2n2gLEVL0JZhwRDnqQ5mL9p+i+Hp66+DqYsHDi+AAIKlQAdSTQClCJAHNeQFaZbyYBEECriF228Oun4yiAAIIFgA+pOq29lEhmapEBQ5sJQHTn07bjJ8/vZrsAUgOMwAAojsFAgLOFhAUjsAkcH6g4OyaQoOM4AAYoIuMSMJ8HKxwbMuyLO0ysYgAKp1B2HqA9fGoLoDIIBAKZDk8T5QswIfn1Jw6torhhsPIU3SU9dfDeamqQFAAIECUJ9UXZpoKY4WKTCueR84EGEBOUiBAkAAsZDTfNGQF8BbZlEDgLJtYOUOcHExiIE9QACxkJOFebnJ8xQopfJxs9LcV5++/qZbygUIIBYGMiaMTgPLKDNNRLl34wFxjgXV3MjNE1qBqWuv0CsADQACiIUcXesP3QMGhBo4e4GyGqidRgwAqdNQEKS5r+jY6BYACCBGYFX8n5JO/vUHH0hqZoD0mGmK08xHp66/BNfi9AIAAcRCkWPJcCgo1WYH69DMQ3sqSR+tAVWCoCIJ1CkARXBO32Gi/QYQQCQHIKgiqIwzwhDfcPAeMGsT7jGAGsagMoomlQcwJxBT9oEiEZITIIGG3IoA5SZSEgZAAJEUgCDLcoCpB1u7T1qEGzxeN9CAmK4lcgUIG6yApcQ9Z56SZB9AALGQmmU1igRxZgNatAdpAWCpDNTLgfV69kz0g1R0JFZAAAHEQqrFoGxK63FAWgPQ2OKUtZfh/JxgXXDkgwKUxObPB4AAAgXgB1LaghuATRh8KQ09ewzG1IfczAEVR6AmGaT9eJlU4y4ABBAoAC8wkDCcBU761/biboPVOg/qQASlPlizC1SZtGeYg2lQ1iWnVQEQQKDBhAfUdCBo3mKQDj+hjGyDA63WCZwCQdl2Cnktg4MAAQQKwIfUduQUGjVTKI7cmZDIhc3ygQIPxAe1+8iM9AcAAQTKwgcYqDyhDsoOsDbWYAGgyg8UYNnACgNWxIBSHmjYjIIccwEggMAT65R05/A1VmFZZDBWJOg1MRngw43lUYIAAQRrxhxgoPIGaJAjQbE70IEISmWg1QywUSNwmw/YWKZCOQ0KMwaAAIIF4EEG6u8gH/BABGVbGk7Gg3bHMwAEEGxWbgMtswtoZHkRHed1QXaCAo7GKxnAKRAggJAXF4FGAhRoaSOo9mtLt6Bplw/Um2hfdI7WA6oHgOWfI4gBEEDMMBER3WBQYHrQuh0Gyla/fv9j0FQQYGBnZaaq2R2LzzG0A/Gbjz9oncgb31xZC96EAxBAA7a8DVRLu5hIg4f4KSkfQcNjIEzMUBqVAMryNoAAQl+hClqNnkDvwh6UpcGDmcD2GaGJelAWBaW209deUqs2JRWgbNIGCKBBvcQXNosHCrBBtDILZYkvQABhW6U/IKlwiADQOTSJyAIAAYRtlX7jaDjhLPswzlcACCCMAIQmzwmj4YUBJmLbuQkQQLh2KoFS4YPRMEOMugADrwGbBEAAYQ1AaEgXjoYbHCTikgAIIJzbXYGBuGE0K0NyI74DegACiNB+YVBWvjCCA+8ArqwLAwABhDcAoVk5EFoDjcRaN5CQIoAAInhuDLRWdhyBgUfUaUYAAUTUwTvQ0ysSR1jgEVV0AQQQ0ScXAQ1cMAICEdz6IOXgHYAAIvnsrGF8AA9Z52cBBNDo4WMUBB4IAAQQEzm2QS0yHCZNHJAfFMkJPBAACKDRAxgpPIARIIBGjwClEAAEEBM1XAPt9hkOka4fqHdlSK3TzwECaPQYZAoBQACNHsRNIQAIoNGj4CkEAAE0ehkBhQAggBgHMm8Nh+swAAIMAJC7vSvyigLFAAAAAElFTkSuQmCC"
            / -->
    </div>
</body>

</html>